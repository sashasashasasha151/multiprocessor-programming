package monotonic_clock

class Solution : MonotonicClock {
    private var reg0_0 by RegularInt(0)
    private var reg0_1 by RegularInt(0)
    private var reg0_2 by RegularInt(0)

    private var reg1_0 by RegularInt(0)
    private var reg1_1 by RegularInt(0)
    private var reg1_2 by RegularInt(0)

    override fun write(time: Time) {
        reg1_0 = time.d1
        reg1_1 = time.d2
        reg1_2 = time.d3

        reg0_2 = time.d3
        reg0_1 = time.d2
        reg0_0 = time.d1
    }

    override fun read(): Time {
        val r_reg0_0 = reg0_0
        val r_reg0_1 = reg0_1
        val r_reg0_2 = reg0_2

        val r_reg1_2 = reg1_2
        val r_reg1_1 = reg1_1
        val r_reg1_0 = reg1_0

        if (r_reg0_0 == r_reg1_0 && r_reg0_1 == r_reg1_1 && r_reg0_2 == r_reg1_2) {
            return Time(r_reg0_0, r_reg0_1, r_reg0_2)
        } else {
            if (r_reg0_0 == r_reg1_0 && r_reg0_1 == r_reg1_1) {
                return Time(r_reg0_0, r_reg0_1, r_reg0_2 + 1)
            } else {
                if (r_reg0_0 == r_reg1_0) {
                    return Time(r_reg0_0, r_reg0_1 + 1, 0)
                } else {
                    return Time(r_reg0_0 + 1, 0, 0)
                }
            }
        }
    }
}