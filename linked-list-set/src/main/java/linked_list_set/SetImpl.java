package linked_list_set;

import java.util.concurrent.atomic.AtomicMarkableReference;

public class SetImpl implements Set {

    private static class Node {

        AtomicMarkableReference<Node> next;
        int x;

        private Node(int x) {
            this.x = x;
        }
    }

    private class Window {
        Node cur, next;
    }

    private Node head;

    public SetImpl() {
        head = new Node(Integer.MIN_VALUE);
        head.next = new AtomicMarkableReference<>(new Node(Integer.MAX_VALUE), false);
    }

    private Window findWindow(int x) {
        Window w = new Window();
        start:
        while (true) {
            w.cur = head;
            w.next = w.cur.next.getReference();
            while (w.next.x < x) {
                Node node = w.next.next.getReference();
                boolean b = w.next.next.isMarked();
                if (b) {
                    if (!w.cur.next.compareAndSet(w.next, node, false, false)) {
                        continue start;
                    }
                    w.next = node;
                } else {
                    w.cur = w.next;
                    w.next = w.cur.next.getReference();
                }
            }

            if (w.next.next == null || !w.next.next.isMarked()) {
                return w;
            }

            w.cur.next.compareAndSet(w.next, w.next.next.getReference(), false, false);
        }
    }

    @Override
    public boolean add(int x) {
        while (true) {
            Window w = findWindow(x);
            if (w.next.x == x) {
                return false;
            }

            Node node = new Node(x);
            node.next = new AtomicMarkableReference<>(w.next, false);
            if (w.cur.next.compareAndSet(w.next, node, false, false)) {
                return true;
            }
        }
    }

    @Override
    public boolean remove(int x) {
        while (true) {
            Window w = findWindow(x);
            if (w.next.x != x) {
                return false;
            }

            Node nextNext = w.next.next.getReference();
            if (w.next.next.compareAndSet(nextNext, nextNext, false, true)) {
                w.cur.next.compareAndSet(w.next, nextNext, false, false);
                return true;
            }
        }
    }

    @Override
    public boolean contains(int x) {
        Window w = findWindow(x);
        return w.next.x == x;
    }
}
